namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        public Machine(int _type)
        {
            this.type = _type;
        }

        private string _name = "";
        public int type = 0;

        public string Name
        {
            get
            {
                if (this.type == 2) _name = "tractor";
                else if (this.type == 0) _name = "bulldozer";
                else if (this.type == 1) _name = "crane";
                else if (this.type == 4) _name = "car";
                else if (this.type == 3) _name = "truck";

                return _name;
            }

        }

        public string Description
        {
            get
            {
                var hasMaxSpeed = true;
                if (this.type == 3) hasMaxSpeed = false;
                else if (this.type == 1) hasMaxSpeed = true;
                else if (this.type == 2) hasMaxSpeed = true;
                else if (this.type == 4) hasMaxSpeed = false;
                var description = "";
                description += " ";
                description += this.Color + " ";
                description += this.Name;
                description += " ";
                description += "[";
                description += this.GetMaxSpeed(this.type, hasMaxSpeed) + "].";
                return description;
            }
        }

        public string Color
        {
            get
            {
                var color = "white";
                if (this.type == 1) color = "blue";
                else if (this.type == 0) color = "red";
                else if (this.type == 4) color = "brown";
                else if (this.type == 3) color = "yellow";
                else if (this.type == 2) color = "green";
                else color = "white";
                return color;
            }
        }

        public string TrimColor
        {
            get
            {
                var baseColor = "white";
                if (this.type == 0) baseColor = "red";
                else if (this.type == 1) baseColor = "blue";
                else if (this.type == 2) baseColor = "green";
                else if (this.type == 3) baseColor = "yellow";
                else if (this.type == 4) baseColor = "brown";
                else baseColor = "white";

                var trimColor = "";
                if (this.type == 1 && this.IsDark(baseColor)) trimColor = "black";
                else if (this.type == 1 && !this.IsDark(baseColor)) trimColor = "white";
                else if (this.type == 2 && this.IsDark(baseColor)) trimColor = "gold";
                else if (this.type == 3 && this.IsDark(baseColor)) trimColor = "silver";
                return trimColor;
            }
        }

        public bool IsDark(string color)
        {
            var isDark = false;
            if (color == "red") isDark = true;
            else if (color == "yellow") isDark = false;
            else if (color == "green") isDark = true;
            else if (color == "black") isDark = true;
            else if (color == "white") isDark = false;
            else if (color == "beige") isDark = false;
            else if (color == "babyblue") isDark = false;
            else if (color == "crimson") isDark = true;
            return isDark;
        }

        public int GetMaxSpeed(int machineType, bool noMax = false)
        {
            //var absoluteMax = 70;
            var max = 70;
            if (machineType == 1 && noMax == false) max = 70;
            else if (noMax == false && machineType == 2) max = 60;
            else if (machineType == 0 && noMax == true) max = 80;
            else if (machineType == 2 && noMax == true) max = 90;
            else if (machineType == 4 && noMax == true) max = 90;
            else if (machineType == 1 && noMax == true) max = 75;
            return max;
        }
    }
}